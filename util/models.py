from django.db import models

class BaseModel(models.Model):
    
    class Meta:
        abstract = True
        
    def to_serializable_dict(self):
        ser_dict = {}
        for key in self.__dict__:
            
            if key.startswith("_"):
                continue
            
            dict_value = self.__dict__.get(key)
            if isinstance(dict_value, basestring) or isinstance(dict_value, (int, long, float, complex)) or  isinstance(dict_value, bool):
                ser_dict[key]=dict_value
        
        return ser_dict
        
        
    def __str__(self):

        list_of_strs = []
        for key in self.__dict__:
            if key.startswith("_"):
                continue
            value = self.__dict__.get(key)
            if isinstance(value, basestring) or isinstance(value, (int, long, float, complex)) or isinstance(value, bool):
                list_of_strs.append("%s : %s" % (key, str(value)))
        
        return "{ %s }" % ', '.join(list_of_strs)
        
    