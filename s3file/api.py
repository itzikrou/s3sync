import json
import logging

from django.conf import settings
from django.conf.urls import url
from django.http.response import HttpResponse
import requests
from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.http import HttpBadRequest
from tastypie.utils.urls import trailing_slash

from s3file.models import S3file, S3syncFolder
from s3file.tasks_extended import local_remote_sync
from sync_setting.models import SyncSettings
from util.api import BaseResource
from web.settings import REMOTE_TASTYPIE_API_NAME


logger = logging.getLogger(__name__)


class S3syncFolderResource(BaseResource):

    class Meta(BaseResource.Meta):
        queryset = S3syncFolder.objects.prefetch_related('s3file_set').all()
        resource_name = 's3syncfolder'
        filtering = {
            "bucket": ALL,
            "path": ALL
        }
        ordering = ["bucket", "path"]
        
    def post_list(self, request, **kwargs):
        req_body = json.loads(getattr(request, '_body', request.body))
        bucket = req_body.get("bucket", None)
        if not bucket:
            if settings.AWS_BUCKET_NAME:
                req_body["bucket"] = settings.AWS_BUCKET_NAME
                setattr(request,"_body",json.dumps(req_body))
            else:
                return HttpBadRequest("No bucket name was provided. Please set the default bucket name in the settings file or send one in the request body.")
        return super(S3syncFolderResource, self).post_list(request, **kwargs)
    
    def dehydrate(self, bundle):
        # Include the request IP in the bundle.
        all_path_s3files = bundle.obj.s3file_set.all()
        bundle.data['uploaded'] = len([1 for s3file in all_path_s3files if s3file.is_file and s3file.upload_succeeded])
        bundle.data['total'] = len([1 for s3file in all_path_s3files if s3file.is_file])
        
        return bundle

class S3fileResource(BaseResource):
    
    path = fields.ToOneField(S3syncFolderResource, 'path', full=True)
        
    class Meta(BaseResource.Meta):
        queryset = S3file.objects.all().select_related("path")
        resource_name = 's3file'
        filtering = {
            "path": ALL_WITH_RELATIONS,
            "is_base_directory": ALL,
            "base_path": ALL,
            "full_path": ALL,
            "basename": ALL,
            "key": ALL,
            "file_size": ALL,
            "mime_type": ALL,
            "duration": ALL,
            "last_updated": ALL,
            "upload_succeeded": ALL,
        }
        ordering = ["path", "is_base_directory", "base_path", "full_path", "basename", "key", "file_size", "mime_type", "duration",  "last_updated",  "upload_succeeded"]
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/verify%s$" % (self._meta.resource_name, trailing_slash()), 
                self.wrap_view('verify_s3file'), 
                name="verify_s3file"),
                
            url(r"^(?P<resource_name>%s)/remote_sync%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remote_sync'), 
                name="remote_sync"),
                
            url(r"^(?P<resource_name>%s)/sync_lib%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('sync_lib'), 
                name="sync_lib"),

                
        ]

    def verify_s3file(self, request, **kwargs):
        # TODO: implement folder verification logic
        return HttpResponse(status=200)
    
    def remote_sync(self, request, **kwargs):
        '''
            local js handler for sending the file list to the remote host
        '''
        settings = SyncSettings.objects.all()
        if not settings.count() == 1:
            return HttpResponse(status=200)
        
        curr_settings = settings[0]
        remote_api_prefix = "api/%s" % REMOTE_TASTYPIE_API_NAME
        url = "http://%s/%s/%s/sync_lib/" % (curr_settings.romote_host_address, remote_api_prefix, self._meta.resource_name)
        data = []
        
        for s3_file in S3file.objects.all().select_related("path"):
            data.append(s3_file.to_serializable_dict())
            
        headers = {'content-type': 'application/json'}
        req = requests.post(url = url, data = json.dumps(data), headers=headers)
        
        return HttpResponse(status=req.status_code)

    def sync_lib(self, request, **kwargs):
        '''
            remote library sync handler
            start a celery task with the pay-load
        '''
        req_body = getattr(request, '_body', request.body)   
        local_remote_sync.apply_async(kwargs={"msg_body" : req_body})  # @UndefinedVariable        
        return HttpResponse(status=200)
