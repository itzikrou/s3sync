# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='S3file',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_base_directory', models.BooleanField(default=False)),
                ('base_path', models.CharField(max_length=1024)),
                ('full_path', models.CharField(max_length=1024)),
                ('basename', models.CharField(max_length=1024)),
                ('is_file', models.BooleanField(default=False)),
                ('key', models.CharField(max_length=1024)),
                ('file_size', models.BigIntegerField(default=0)),
                ('mime_type', models.CharField(max_length=1024, null=True)),
                ('duration', models.CharField(max_length=1024, null=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('upload_succeeded', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='S3syncFolder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bucket', models.CharField(max_length=1024)),
                ('path', models.CharField(max_length=1024)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='s3file',
            name='path',
            field=models.ForeignKey(to='s3file.S3syncFolder'),
            preserve_default=True,
        ),
    ]
