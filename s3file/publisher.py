'''
Created on Oct 24, 2014

@author: iRousso
'''

import logging
import pika
from django.conf import settings
import json


logger = logging.getLogger(__name__)

class Publisher(object):

    @classmethod
    def publishToQueue(cls, msg_body, queue):
        
        connection = pika.BlockingConnection(pika.URLParameters(settings.BROKER_URL))
        
        channel = connection.channel()
        channel.queue_declare(queue=queue)
        channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(msg_body))
        
        logger.info(" sent msg: %s to queue: %s", msg_body, queue)
        connection.close()