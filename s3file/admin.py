from django.contrib import admin

from s3file.models import S3file, S3syncFolder


admin.site.register(S3file)
admin.site.register(S3syncFolder)
