from django.db import models

from util.models import BaseModel

MAX_LENGTH=1024

class S3syncFolder(BaseModel):
    bucket = models.CharField(max_length=MAX_LENGTH)
    path = models.CharField(max_length=MAX_LENGTH)

class S3file(BaseModel):
    path = models.ForeignKey(S3syncFolder)
    is_base_directory = models.BooleanField(default=False)
    base_path = models.CharField(max_length=MAX_LENGTH)
    full_path = models.CharField(max_length=MAX_LENGTH)
    basename = models.CharField(max_length=MAX_LENGTH)
    is_file = models.BooleanField(default=False)
    key = models.CharField(max_length=MAX_LENGTH)
    file_size = models.BigIntegerField(default=0)
    mime_type = models.CharField(max_length=MAX_LENGTH, null=True)
    duration = models.CharField(max_length=MAX_LENGTH, null=True)
    last_updated = models.DateTimeField(auto_now=True)
    upload_succeeded = models.BooleanField(default=False)
    
    def to_serializable_dict(self):
        serializable_dict = super(S3file, self).to_serializable_dict()
        serializable_dict.pop("id", None)
        serializable_dict.pop("path_id", None)
        path_dict = self.path.to_serializable_dict()
        path_dict.pop("id", None)
        serializable_dict["path"] = path_dict
        return serializable_dict
