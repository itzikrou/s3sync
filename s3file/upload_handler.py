import json
import logging

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings

from s3file.models import S3file


logger = logging.getLogger(__name__)

class UploadHandler:
    
    @classmethod
    def handle_msg(cls, channel, method, properties, body):
        
        # TODO: add pause mode logic
        logger.info("got msg: %s", body)
        
        msg = json.loads(body)
        file_id_to_upload = msg.get("file_id_to_upload", None)
        if not file_id_to_upload:
            logger.warning("did not receive a valid file id to upload. returning.")
            return
        
        try:
            cls.upload_to_s3(file_id_to_upload)
        except Exception:
            logger.exception("got an exception while processing %s" % file_id_to_upload)
            
    
    @classmethod
    def mycb(cls, so_far, total):
        logger.debug('%d bytes transferred out of %d - %s%%' % (so_far, total, str(100 * float(so_far)/float(total)) ) )

                    
    @classmethod
    def upload_to_s3(cls, file_id):
        try:
            s3_file = S3file.objects.get(key=file_id)
            conn = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
            bucket = conn.create_bucket(s3_file.path.bucket)
            
            
            k = Key(bucket = bucket)
            k.key = s3_file.key
            content_disposition = "attachment;filename=%s" % s3_file.basename
            k.content_disposition=content_disposition
            # TODO: add upload progress support
            logger.info("uploading %s:", file_id)
            k.set_metadata('Content-Disposition', content_disposition)
            k.set_contents_from_filename(s3_file.full_path, cb=cls.mycb, num_cb=20)            
            k.set_acl('public-read')
            logger.info("file with sha256 %s uploaded successfully.", s3_file.key)
            s3_file.upload_succeeded = True
            s3_file.save()            
        
        except S3file.DoesNotExist:
            logger.info("file with sha256 %s does not exist in the DB!.", file_id)
