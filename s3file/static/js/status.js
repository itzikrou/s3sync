function render_status_table(data){
	
	console.log("rendering table..")	
	$("#status_table_body").html(_.template($("#status_table_row_template").html(),data));
}

function fetch_status() {
	
	http_handler.get("s3syncfolder/", render_status_table);
	
}

$( document ).ready(function() {
	
	$(document).on('opened.fndtn.reveal', '[data-reveal]', function (e) {
		
		// ignore non-namespaced event
    	// see: https://github.com/zurb/foundation/issues/5482
        if (e.namespace != 'fndtn.reveal') return;
        
		  var modal = $(this);
		  if(modal.attr('id') == "status"){
			  fetch_status();
		  }
		  
		});
});