function get_s3_base_dirs() {
	
	http_handler.get("s3file/?is_base_directory=true", function(data) {
		
		var render_data = {
				objects:data.objects,
				header:""
		}
		
		// console.log(render_data);
		
		render_s3_files_table(render_data);
	});
}

function get_sub_dir_content(path) {
	
	http_handler.get("s3file/?base_path=" + path, function(data) {
		
		var render_data = {
				objects:data.objects,
				header:path
		}
		
		// console.log(render_data);
		
		render_s3_files_table(render_data);
	});
	
}

function render_s3_files_table(data){
	
	var data_to_render = _.reject(data.objects, function(s3obj) {
		
		var should_be_filtered = false;
		var regexs = [/(^\.)/];
		
		for(var i=0; i<regexs.length; i++){
			var curr_regex = regexs[i];
			if(curr_regex.test(s3obj.basename)){
				should_be_filtered = true;
				break;
			}
		}
		
		return should_be_filtered; 
	});
	
	data.objects = data_to_render;
	
	var rendered_html = _.template($("#table_template").html(), data);
	$("#s3files_table").html(rendered_html);	
}

function get_sub_dir_files(click_obj){
	
	var data = $(click_obj).data();
	// console.log(data);
	get_sub_dir_content(data.dir_path);
	
}

function download_file(click_obj){
	var data = $(click_obj).data();
	// console.log(data);	
}

function build_partail_url(dir_path, i, delimiter){
	var url = "";
	for(var j=0; j<i; j++){
		url = url + dir_path[j] + delimiter;
	}
	
	url = url + dir_path[i];
	
	return url;
	
}

function build_header_urls(header){
	// /Users/iRousso/test/Podcasts/News (HD)
	if(typeof(header) != "string"){
		return "";
	}
	
	// TODO: add support for windows :)
	var delimiter = "/";
	var dir_path = header.split(delimiter);
	var ulred_header = "";
	var urled_parts = [];
	
	for(var i=0; i< dir_path.length ;i++) {
		var curr_path = dir_path[i];
		var partial_url = build_partail_url(dir_path, i, delimiter);
		
		urled_parts.push("<a href='#' data-dir_path='" + partial_url + "' onclick='get_sub_dir_files(this)'>" + curr_path +"</a>")
	}
	
	return urled_parts.join(delimiter);
}

$( document ).ready(function() {	
	get_s3_base_dirs();
});