function verify_path(){
	
	var req_data = {
			"absolute_path" : $("#absolute_path").val()
	};
	
	http_handler.post("s3file/verify/", req_data, function(data) {
		
		// console.log("verify_path done handler");
		$(".button.success").removeClass("disabled");
		
	});
}

function set_folder_to_sync(clickObj){
	
	if($(clickObj).hasClass("disabled")){
		// console.log('disabled!');
		return;
	}
	
	var req_data = {
			"path" : $("#absolute_path").val(),
			"bucket": ""
	};
	
	http_handler.post("s3syncfolder/", req_data, function(data) {
		
		// console.log("set_folder_to_sync done handler");
		$('#add_folder').foundation('reveal', 'close');
	});
	
}


$( document ).ready(function() {
	
	$('#add_folder_form')
	  .on('invalid.fndtn.abide', function() {
		  
	    var invalid_fields = $(this).find('[data-invalid]');
	    // console.log(invalid_fields);
	    
	  })
	  .on('valid.fndtn.abide', function() {
		  
	    // console.log('valid!');
	    verify_path();
	  });
	
	$("#absolute_path").keyup(function() {
		
		if(!$(".button.success").hasClass("disabled")){
			$(".button.success").addClass("disabled");
		}
	});
	
});