from django.conf import settings
from django.shortcuts import render_to_response


def s3files(request, *args, **kwargs):
    context = {'REMOTE_HOST_ADDRESS':settings.REMOTE_HOST_ADDRESS}
    return render_to_response('index.html', context)