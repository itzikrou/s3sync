'''
Created on Oct 24, 2014

@author: iRousso
'''

from hachoir_parser import createParser
from hachoir_metadata import extractMetadata
import logging

logger = logging.getLogger(__name__)

class FileMetaDataParser(object):
    '''
    Helper class for fetching meta data on files
    '''
    file_name = None

    def __init__(self, file_name):
        '''
        Constructor
        '''
        self.file_name = file_name
    
    def get_file_details(self):
        
        details = {}
        
        if not self.file_name:
            return details
        
        parser = createParser(self.file_name, self.file_name)
        if not parser:
            logger.info("Unable to parse file %s", self.file_name)
            return details
        
        try:
            metadata = extractMetadata(parser)
        except Exception as err:
            logger.info("Metadata extraction error: %s" % unicode(err))
            metadata = details
        
        if not metadata:
            logger.info("Unable to extract metadata")
            return details
        
        text = metadata.exportPlaintext()        
        for line in text:
            line = line.lower().replace("- ", "").split(":")
            if len(line) == 2:
                details[line[0].strip().replace(" ", "_")] = line[1].strip().replace(" ", "_")
                
        return details