'''
django command has only one requirement: 
    it must define a class Command that extends BaseCommand or one of its subclasses.
'''

import logging

from django.conf import settings
from django.core.management.base import BaseCommand
import pika

from s3file.upload_handler import UploadHandler


logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'listens to messages on the %s queue' % settings.FILE_UPLOAD_QUEUE_NAME

    def handle(self, *args, **options):
        
        connection = pika.BlockingConnection(pika.URLParameters(settings.BROKER_URL))    
        channel = connection.channel()
        channel.queue_declare(queue=settings.FILE_UPLOAD_QUEUE_NAME)
        
        logger.info("starting a message listener on %s - %s", settings.BROKER_URL, settings.FILE_UPLOAD_QUEUE_NAME)
         
        channel.basic_consume(UploadHandler.handle_msg, queue=settings.FILE_UPLOAD_QUEUE_NAME, no_ack=True)
        
        channel.start_consuming()

        
                
