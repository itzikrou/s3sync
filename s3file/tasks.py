
import json
import logging

from celery import task
from celery.utils import deprecated
from django.conf import settings

from s3file.management.commands.syncFolders import SyncHandler
from s3file.models import S3syncFolder
from s3file.publisher import Publisher
from sync_setting.models import SyncSettings


logger = logging.getLogger(__name__)

@task()
@deprecated
def sync_folders_rabbitmq():
    logger.info("running task publishToQueue")
    
    folders_to_sync = S3syncFolder.objects.all()
    
    if folders_to_sync.count() == 0:
        logger.info("no folders to sync on. returning.")
        return
    
    for folder in folders_to_sync:
        msg_body = {
                        "bucket_name": folder.bucket,
                        "path_to_sync": folder.path
        }
        
        Publisher.publishToQueue(msg_body, settings.FOLDER_SYNC_QUEUE_NAME)
        logger.info("published sync task with : bucket: %s path: %s" ,folder.bucket, folder.path)

@task()
def sync_folders():
    
    logger.info("running periodic celery folder sync task")
    
    settings = SyncSettings.objects.all()
    if settings.count() ==  1:
        curr_settings = settings[0]
        if not curr_settings.sync_enabled:
            logger.info("folder sync is not enbaled. returning.")
            return
    
    logger.info("folder sync is enbaled, syncing...")
    
    folders_to_sync = S3syncFolder.objects.all()
    
    if folders_to_sync.count() == 0:
        logger.info("no folders to sync on. returning.")
        return
    
    for folder in folders_to_sync:
        msg_body = {
                        "bucket_name": folder.bucket,
                        "path_to_sync": folder.path
        }
        
        sync_folders_async.apply_async(kwargs={"msg_body" : json.dumps(msg_body)})
        logger.info("published sync task with : bucket: %s path: %s" ,folder.bucket, folder.path)
        
        
@task()
def sync_folders_async(*args, **kwargs):
    logger.info("running celery task wrapper to sync_folders")
    SyncHandler.callback(ch=None, method=None, properties=None, body=kwargs.get("msg_body"))
    
    
