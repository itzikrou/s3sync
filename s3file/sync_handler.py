import hashlib
import json
import logging
import os

from django.conf import settings

from s3file.fileMetaDataParser import FileMetaDataParser
from s3file.models import S3syncFolder, S3file
from s3file.publisher import Publisher
from s3file.tasks_extended import upload_files_async


logger = logging.getLogger(__name__)

class SyncHandler:
    
    @classmethod
    def callback(cls, ch, method, properties, body):
        
        logger.debug("Received message %s" % body)
        
        msg = json.loads(body)
        bucket_name = msg.get("bucket_name", None)
        if not bucket_name:
            logger.warning("did not receive a valid bucket name. returning.")
            return
        
        path_to_sync = msg.get("path_to_sync", None)        
        if not path_to_sync:
            logger.warning("did not receive any path to sync. returning.")
            return
        
        logger.info("syncing path: %s", path_to_sync)

        if not (os.path.isfile(path_to_sync) or os.path.isdir(path_to_sync)):
            logger.warning("did not receive a valid path to sync. returning")
            return
            
        folder_to_sync = S3syncFolder.objects.get(bucket = bucket_name, path = path_to_sync)
        # upload all files under that directory
        cls.handle_dir_upload(folder_to_sync)
                    
    @classmethod
    def handle_file_upload(cls, folder_to_sync, root, fullpath, with_upload = True):
        sha256 = cls.get_hash(fullpath)
        logger.info("processing file at path: %s with sha256: %s",  fullpath, sha256)
        try:
            #TODO: optimize the case of identical file in separate directory path
            s3_db_file = S3file.objects.get(full_path = fullpath)
            # this file (pull path) was processed before                
            if s3_db_file.key == sha256:                    
                if s3_db_file.upload_succeeded:
                    logger.info("file is unchanged and uploaded, returning...")
                    return
                else:
                    logger.info("file is unchanged but not uploaded, processing...")
                    if with_upload:
                        cls.publish_upload_task(sha256)                

            else:
                logger.info("file has changed, processing...")
                if with_upload:
                    cls.publish_upload_task(sha256)

        except S3file.DoesNotExist:
            
            logger.info("found a new file, processing...")
            s3_file = S3file()
            s3_file.path = folder_to_sync
            
            file_info = os.stat(fullpath)
            
            if os.path.isfile(fullpath):          
                try:
                    parser = FileMetaDataParser(fullpath)
                    details = parser.get_file_details()
                    s3_file.is_file = True
                    s3_file.mime_type = details.get("mime_type", None)
                    s3_file.duration = details.get("duration", None)
                except Exception:
                    logger.exception("could not parse meta-data for file: %s " % fullpath)
            else:
                s3_file.is_file = False
            
            s3_file.bucket = folder_to_sync.bucket
            
            if folder_to_sync.path == fullpath:
                s3_file.is_base_directory = True
                
            s3_file.base_path = root            
            s3_file.full_path = fullpath
            s3_file.basename = os.path.basename(fullpath)
            
            s3_file.key = sha256
            s3_file.file_size = file_info.st_size
            
            s3_file.save()
            
            if with_upload:
                cls.publish_upload_task(sha256)
    
    @classmethod
    def handle_dir_upload(cls, folder_to_sync):
        
        for root, dirs_in_root, files_in_root in os.walk(folder_to_sync.path):  # @UnusedVariable
            '''
            for each root directory:
                build sub directory hierarchy
                upload current level files
            '''
            # create an entry in the db for the base directory
            cls.handle_file_upload(folder_to_sync, os.path.abspath(os.path.join(root, os.pardir)), root, False)
            
            for file_to_process in files_in_root:
                fullpath = os.path.join(root, file_to_process)
                # TODO: add filter for system files (configurable?)
                cls.handle_file_upload(folder_to_sync, root, fullpath)
        
    @classmethod
    def get_hash(cls,file_name, blocksize=65536):
        hasher = hashlib.sha256()
        hasher.update(file_name)
        if os.path.isfile(file_name):            
            with open(file_name) as afile:
                buf = afile.read(blocksize)
                while len(buf) > 0:
                    hasher.update(buf)
                    buf = afile.read(blocksize)                
            return hasher.hexdigest()
        else:
            return hasher.hexdigest()
    
    @classmethod
    def publish_upload_rabbitmq_task(cls,sha256):
        msg_body = {
                    "file_id_to_upload": sha256
        }
        
        Publisher.publishToQueue(msg_body, settings.FILE_UPLOAD_QUEUE_NAME)


    @classmethod
    def publish_upload_task(cls,sha256):
        msg_body = {
                    "file_id_to_upload": sha256
        }
        
        upload_files_async.apply_async(kwargs={"msg_body" : json.dumps(msg_body)})  # @UndefinedVariable
