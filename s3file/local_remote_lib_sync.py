import json
import logging

from s3file.models import S3file, S3syncFolder


logger = logging.getLogger(__name__)

class LocalRemoteSync:
    
    @classmethod
    def callback(cls, ch, method, properties, body):
        
        msg = json.loads(body)
        for remote_s3_file in msg:
            loc_path = remote_s3_file.pop("path", None)
            
            if loc_path:
                local_s3_path, created = S3syncFolder.objects.get_or_create(path=loc_path.get("path"), defaults=loc_path)
                if created:
                    logger.debug("created a copy of %s" % local_s3_path.to_serializable_dict())
                    
                remote_s3_file["path"] = local_s3_path
                local_s3_file, created = S3file.objects.get_or_create(key=remote_s3_file.get("key"), defaults=remote_s3_file)
                if not created:
                    continue
            
                logger.debug("created a copy of %s" % local_s3_file.to_serializable_dict())
