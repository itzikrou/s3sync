import logging

from celery import task

from s3file.local_remote_lib_sync import LocalRemoteSync
from s3file.upload_handler import UploadHandler


logger = logging.getLogger(__name__)

# this task is separated to avoid import loop
@task()
def upload_files_async(*args, **kwargs):
    logger.info("running celery task wrapper to upload_files_async")
    UploadHandler.handle_msg(channel=None, method=None, properties=None, body=kwargs.get("msg_body"))



@task()
def local_remote_sync(*args, **kwargs):
    logger.info("running celery task wrapper to local_remote_sync")
    LocalRemoteSync.callback(ch=None, method=None, properties=None,body=kwargs.get("msg_body")) 
