function HttpHandler(api_prefix) {

	this.api_prefix = api_prefix;
	$.ajaxSetup({
		  contentType: "application/json; charset=utf-8",
		  processData:false
		});
}

HttpHandler.prototype.build_url = function(url, req_type){
	
	var res_url =  this.api_prefix + url;
	
	if(req_type == "get") {
		
		if(url.indexOf("?") != -1){
			// the url already contains a "?"
			res_url =  res_url + "&format=json";
		}
		else{
			res_url =  res_url + "?format=json";
		}
	}
	
	return res_url;
}

HttpHandler.prototype.get = function(url, on_success, on_fail) {
	
	$.ajax({
		  type: "GET",
		  url: this.build_url(url, "get"),		  
		  success : on_success
		})
	.done(function(data) {
		
	  })
    .fail(function(jqXHR, textStatus, errorThrown) {
    	
    	console.log("request failed.");
    	console.log(jqXHR);
    	
    	if(typeof(on_fail) != "undefined"){
    		on_fail(jqXHR);
    	}
    })
    .always(function(responseObj, textStatus, httpObj) {
    	
    });

}

HttpHandler.prototype.post = function(url, data, on_success, on_fail) {
	
	$.ajax({
		  type: "POST",
		  url: this.build_url(url, "post"),
		  data: JSON.stringify(data),
		  success : on_success
		})
	.fail(function(jqXHR, textStatus, errorThrown) {
    	
    	console.log("request failed.");
    	console.log(jqXHR);
    	
    	if(typeof(on_fail) != "undefined"){
    		on_fail(jqXHR);
    	}
    })
    .always(function(responseObj, textStatus, httpObj) {
    	
    });

}

HttpHandler.prototype.put = function(url, data, on_success, on_fail) {
	
	$.ajax({
		  type: "PUT",
		  url: this.build_url(url, "put"),
		  data: JSON.stringify(data),
		  success : on_success
		})
	.fail(function(jqXHR, textStatus, errorThrown) {
    	
    	console.log("request failed.");
    	console.log(jqXHR);
    	
    	if(typeof(on_fail) != "undefined"){
    		on_fail(jqXHR);
    	}
    })
    .always(function(responseObj, textStatus, httpObj) {
    	
    });

}