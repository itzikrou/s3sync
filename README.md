S3 Sync
========

Automatically backs up (and syncs) a local directory to Amazon's S3 service.
(or: cut the dropbox middle-man out)

Helpers
=======

watch for sass changes: bundle exec compass watch

delete the remote s3 bucket: aws s3 rb s3://<bucket-name> --force

