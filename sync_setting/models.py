from django.db import models

from s3file.models import MAX_LENGTH
from util.models import BaseModel


class SyncSettings(BaseModel):
    sync_enabled = models.BooleanField(default=True)
    romote_host_address = models.CharField(max_length=MAX_LENGTH)