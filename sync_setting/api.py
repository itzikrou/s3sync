from sync_setting.models import SyncSettings
from util.api import BaseResource


class SyncSettingsResource(BaseResource):
    class Meta(BaseResource.Meta):
        queryset = SyncSettings.objects.all()
        resource_name = 'sync_settings'
