# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations


def insert_degault_settings(apps, schema_editor):    
    SyncSettings = apps.get_model("sync_setting", "SyncSettings")
    SyncSettings(sync_enabled=True, romote_host_address=settings.REMOTE_HOST_ADDRESS).save()


class Migration(migrations.Migration):

    dependencies = [
        ('sync_setting', '0001_initial'),
    ]

    operations = [
                  migrations.RunPython(insert_degault_settings),
    ]
