function sync_remote_server(clickObj){
	
	http_handler.post("s3file/remote_sync/", {}, function(data) {
		
		console.log(data);
		
	});	
	
}

function handle_sync_sataus_change(currentValue) {
    
	console.log(currentValue);
    if(typeof s3_settings != "undefined"){
    	s3_settings.sync_enabled = (currentValue === 'true');
    }
	
	http_handler.put("sync_settings/" + s3_settings.id + "/", s3_settings, function(data) {
		
		console.log(data);
		
	});
}

function handle_remove_host_haddress_changed(clickObj){
	
	romote_host_address = $("#romote_host_address").val();
	console.log(romote_host_address);
	
	if(typeof s3_settings != "undefined"){
    	s3_settings.romote_host_address = romote_host_address;
    }
	
	http_handler.put("sync_settings/" + s3_settings.id + "/", s3_settings, function(data) {
		
		console.log(data);
		
	});
}

function populate_settings_form(data){
	
	console.log("populating form..");
	
	if(typeof data != "undefined" && 
		typeof data.objects != "undefined" && 
		data.objects.length == 1){
			var settings = data.objects[0];
			console.log(settings);
			s3_settings = settings;
			
			$("#romote_host_address").val(settings.romote_host_address);			
			if(settings.sync_enabled == false){
				$("#enabedFalse").attr('checked', 'checked');
			}
	}
	
}

function fetch_settings() {
	
	http_handler.get("sync_settings/", populate_settings_form);
	
}

$( document ).ready(function() {
	
	$(document).on('opened.fndtn.reveal', '[data-reveal]', function (e) {
		
		// ignore non-namespaced event
    	// see: https://github.com/zurb/foundation/issues/5482
        if (e.namespace != 'fndtn.reveal') return;
        
		  var modal = $(this);
		  if(modal.attr('id') == "settings"){
			  fetch_settings();
		  }
		  
		});
	
	$('input[type=radio][name=syncStatus]').change(function() {
		handle_sync_sataus_change(this.value);
    });
});