from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from tastypie.api import Api

from s3file.api import S3fileResource, S3syncFolderResource
from s3file.views import s3files
from sync_setting.api import SyncSettingsResource
from web.settings import TASTYPIE_API_NAME


v1_api = Api(api_name=TASTYPIE_API_NAME)
v1_api.register(S3fileResource())
v1_api.register(S3syncFolderResource())
v1_api.register(SyncSettingsResource())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^$', s3files, name='s3files'),
)

urlpatterns += staticfiles_urlpatterns()
