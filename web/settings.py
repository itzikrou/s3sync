"""
Django settings for web project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'j+302kua4c=_%(4=!8$61h=6e1iim-=n=0*-3iog1ua9-vcug='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'gunicorn',
    'djcelery',
    'kombu.transport.django',
    'tastypie',

    's3file',
    'sync_setting',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'web.urls'

WSGI_APPLICATION = 'web.wsgi.application'

TASTYPIE_API_NAME='v1'
REMOTE_TASTYPIE_API_NAME = TASTYPIE_API_NAME

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, "common_static"),
)

STATIC_ROOT = os.path.join(BASE_DIR, "../static/s3sync/")

TEMPLATE_DIRS = (
                 os.path.join(BASE_DIR, "common_template"),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
            'verbose': {
                'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt' : "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
     },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 's3sync.log',
            'formatter': 'verbose'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django': {
            'handlers':['file', 'console'],
            'propagate': False,
            'level':'INFO',
        },
      'gunicorn': {
            'handlers': ['file', 'console'],
            'propagate': True,
            'level': 'INFO',
        },
      's3file': {
            'handlers': ['file', 'console'],
            'propagate': True,
            'level': 'DEBUG',
        },      
      'pika': {
            'handlers': ['file', 'console'],
            'propagate': False,
            'level': 'INFO',
        },

    }
}


# celery configuration:
import djcelery
from celery.schedules import crontab
djcelery.setup_loader()

CELERYBEAT_SCHEDULE = {
    # Executes every minute
    'sync_folders': {
        'task': 's3file.tasks.sync_folders',
        'schedule': crontab(minute='*'),
    },
}

CELERY_TIMEZONE = 'UTC'

########## fill these parameters in local_settings.py ##############

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 's3sync.db',
    }
}

BROKER_URL = 'django://'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
FOLDER_SYNC_QUEUE_NAME='s3_folder_sync'
FILE_UPLOAD_QUEUE_NAME='s3_file_upload'

# Amazon keys
AWS_ACCESS_KEY_ID=''
AWS_SECRET_ACCESS_KEY=''

# Amazon bucket name
AWS_BUCKET_NAME=''

# a remote (globally accessible) s3sync server to sync the library data to
REMOTE_HOST_ADDRESS=''

try:
    from local_settings import *
except ImportError:
    pass